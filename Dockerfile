FROM ubuntu
RUN apt-get update
RUN echo 1.0 >> /etc/version && apt-get install -y git \
	&& apt-get install -y iputils-ping
##WORKDIR##
RUN mkdir /datos1
WORKDIR /datos1
RUN touch datos1.txt
RUN mkdir /datos2
WORKDIR /datos2
RUN touch datos2.txt

##COPY##
COPY index.html .
COPY app.log /datos1

##ENV##
ENV x=10
ENV dir=/data dir1=/data1
RUN mkdir $dir && mkdir $dir1

##ARG PONER VARIABLES AL MOMENTO DE CREAR EL CONTENEDOR##
#ARG dir2
#RUN mkdir $dir2
#ARG user
#ENV user_docker $user
#ADD add_user.sh /datos1
#RUN /datos1/add_user.sh

##EXPOSE##
RUN apt-get install -y apache2
EXPOSE 80
ADD entrypoint.sh /datos1

##VOLUME##
ADD paginas /var/www/html
VOLUME ["/var/wwww/html"]

##CMD##
CMD /datos1/entrypoint.sh

##ENTRYDATE##
#ENTRYPOINT ["/bin/bash"]
